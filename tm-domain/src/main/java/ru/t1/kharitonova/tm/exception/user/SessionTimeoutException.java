package ru.t1.kharitonova.tm.exception.user;

import ru.t1.kharitonova.tm.exception.AbstractException;

public class SessionTimeoutException extends AbstractException {

    public SessionTimeoutException() {
        super("Error! Session timeout.");
    }

}
