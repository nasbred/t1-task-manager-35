package ru.t1.kharitonova.tm.api.service;

import ru.t1.kharitonova.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
