package ru.t1.kharitonova.tm.api.repository;

import ru.t1.kharitonova.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project>{
}
