package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.repository.ISessionRepository;
import ru.t1.kharitonova.tm.api.service.ISessionService;
import ru.t1.kharitonova.tm.model.Session;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final ISessionRepository repository) {
        super(repository);
    }

}
