package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.api.repository.IUserRepository;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.field.NameEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.repository.ProjectRepository;
import ru.t1.kharitonova.tm.repository.TaskRepository;
import ru.t1.kharitonova.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @NotNull
    private List<Project> projectList;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private List<User> userList;

    @Before
    public void init() {
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        userList = new ArrayList<>();

        @NotNull final User userAdmin = new User();
        @NotNull final String adminId = userAdmin.getId();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash("admin");
        userAdmin.setEmail("admin@test.ru");
        userAdmin.setRole(Role.ADMIN);

        @NotNull final User userTest = new User();
        @NotNull final String userId = userTest.getId();
        userTest.setLogin("test");
        userTest.setPasswordHash("test");
        userTest.setEmail("test@test.ru");
        userTest.setRole(Role.USUAL);

        userService.add(userAdmin);
        userService.add(userTest);
        userList.add(userAdmin);
        userList.add(userTest);

        projectList.add(new Project(adminId, "Project 1", "Description 1 admin", Status.IN_PROGRESS));
        projectList.add(new Project(adminId, "Project 2", "Description 2 admin", Status.NOT_STARTED));
        projectList.add(new Project(userId, "Project 3", "Description 3 user", Status.IN_PROGRESS));
        projectList.add(new Project(userId, "Project 4", "Description 4 user", Status.NOT_STARTED));

        taskList.add(new Task(adminId, "Task 1", "Description 1 admin", Status.NOT_STARTED, projectList.get(0).getId()));
        taskList.add(new Task(adminId, "Task 2", "Description 2 admin", Status.IN_PROGRESS, projectList.get(1).getId()));
        taskList.add(new Task(userId, "Task 3", "Description 3 user", Status.IN_PROGRESS, projectList.get(2).getId()));
        taskList.add(new Task(userId, "Task 4", "Description 4 user", Status.IN_PROGRESS, null));

        for (@NotNull final Project project : projectList) projectService.add(project);
        for (@NotNull final Task task : taskList) taskService.add(task);
    }

    @After
    public void clear() {
        projectList.clear();
        taskList.clear();
        userList.clear();
    }

    @Test
    public void testAdd() {
        @NotNull final Project project = new Project(
                userList.get(0).getId(),
                "Test Project",
                "Test Add Project",
                Status.NOT_STARTED);
        projectService.add(project);
        Assert.assertTrue(projectService.existsById(project.getId()));
    }

    @Test
    public void testAddAll() {
        @NotNull List<Project> projects = new ArrayList<>();
        final int projectSize = projectService.getSize();
        for (int i = 0; i < projectSize; i++) {
            projects.add(new Project());
        }
        projectService.addAll(projects);
        Assert.assertEquals(projectSize * 2, projectService.getSize());
    }

    @Test
    public void testCreate() {
        int projectSize = projectList.size();
        int i = 0;
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final String name = "Project_" + user.getLogin();
            @NotNull final String description = "Description_" + user.getLogin();
            @NotNull final Project project = projectService.create(name, userId, description);
            Assert.assertNotNull(project);
            i++;
        }
        Assert.assertEquals(projectSize + i, projectService.getSize());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithNullUser() {
        Assert.assertNotNull(projectService.create(
                null,
                "Test Project",
                "Test Description")
        );
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithNullName() {
        Assert.assertNotNull(projectService.create(
                userList.get(0).getId(),
                null,
                "Test Description")
        );
    }

    @Test
    public void testChangeStatusByIdPositive() {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            projectService.changeProjectStatusById(userId, id, Status.COMPLETED);
            @Nullable final Project actualProject = projectService.findOneById(userId, id);
            Assert.assertNotNull(actualProject);
            @NotNull final Status actualStatus = actualProject.getStatus();
            Assert.assertEquals(Status.COMPLETED, actualStatus);
        }
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeStatusByIdNegative() {
        @NotNull final String userId = "Other_user_id";
        @NotNull final String id = "Other_id";
        projectService.changeProjectStatusById(userId, id, Status.COMPLETED);
    }

    @Test
    public void testChangeStatusByIndexPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> userProjects = projectService.findAll(userId);
            for (int i = 0; i < userProjects.size(); i++) {
                @Nullable final Project project = projectService.findOneByIndex(userId, i);
                Assert.assertNotNull(project);
                projectService.changeProjectStatusByIndex(userId, i, Status.IN_PROGRESS);
                @NotNull final Status status = project.getStatus();
                Assert.assertEquals(Status.IN_PROGRESS, status);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusByIndexNegative() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> userProjects = projectService.findAll(userId);
            projectService.changeProjectStatusByIndex(userId, userProjects.size() + 1, Status.IN_PROGRESS);
        }
    }

    @Test
    public void testExistsByIdPositive() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            Assert.assertTrue(projectService.existsById(id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertFalse(projectService.existsById("Other_Id"));
        Assert.assertFalse(projectService.existsById("Other_user_id", "Other_Id"));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> projects = projectService.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectList.size(), projects.size());
    }

    @Test
    public void testFindAllByUserId() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> actualProjects = projectService.findAll(userId);
            @NotNull final List<Project> expectedProjects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            Assert.assertEquals(expectedProjects, actualProjects);
        }
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            Assert.assertEquals(project, projectService.findOneById(id));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = "Other_id";
        @Nullable final Project project = projectService.findOneById(id);
        Assert.assertNull(project);
    }

    @Test
    public void testFindOneByIdUserPositive() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            @Nullable final String userId = project.getUserId();
            Assert.assertEquals(project, projectService.findOneById(userId, id));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        @NotNull final String id = "Other_Id";
        @NotNull final String userId = "Other_UserId";
        @Nullable final Project project = projectService.findOneById(userId, id);
        Assert.assertNull(project);
    }

    @Test
    public void testFindOneByIndexPositive() {
        for (int i = 0; i < projectList.size(); i++) {
            @Nullable final Project actualProject = projectService.findOneByIndex(i);
            @Nullable final Project expectedProject = projectList.get(i);
            Assert.assertNotNull(actualProject);
            Assert.assertEquals(expectedProject, actualProject);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindOneByIndexNegative() {
        final int index = projectList.size() + 1;
        Assert.assertNull(projectService.findOneByIndex(index));
    }

    @Test
    public void testFindByIndexForUserPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> projects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < projects.size(); i++) {
                Assert.assertNotNull(projectService.findOneByIndex(userId, i));
                @Nullable final Project actualProject = projectService.findOneByIndex(userId, i);
                Assert.assertNotNull(actualProject);
                @Nullable final Project expectedProject = projects.get(i);
                Assert.assertEquals(expectedProject, actualProject);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexForUserNegative() {
        @NotNull final String userId = "Other_User_Id";
        final int index = projectList.size() + 1;
        Assert.assertNull(projectService.findOneByIndex(userId, index));
    }

    @Test
    public void testGetSize() {
        final int projectSize = projectService.getSize();
        Assert.assertEquals(projectList.size(), projectSize);
    }

    @Test
    public void testGetSizeForUser() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            final int actualProjectSize = projectService.getSize(userId);
            @NotNull final List<Project> expectedProjects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            final int expectedProjectSize = expectedProjects.size();
            Assert.assertEquals(expectedProjectSize, actualProjectSize);
        }
    }

    @Test
    public void testRemoveOne() {
        for (int i = 0; i < projectList.size(); i++) {
            @NotNull final Project project = projectList.get(i);
            projectRepository.removeOne(project.getUserId(), project);
            Assert.assertEquals(projectRepository.getSize(), projectList.size() - i - 1);
        }
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void testRemoveOneByIdPositive() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            Assert.assertNotNull(projectService.removeOneById(id));
        }
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void testRemoveOneByIdNegative() {
        @NotNull final String id = "Other_Id";
        Assert.assertNull(projectService.removeOneById(id));
        Assert.assertEquals(projectList.size(), projectService.getSize());
    }

    @Test
    public void testRemoveOneByIndexPositive() {
        for (int i = 0; i < projectList.size(); i++) {
            Assert.assertNotNull(projectService.removeOneByIndex(0));
        }
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexNegative() {
        @NotNull final Integer index = projectList.size() + 1;
        Assert.assertNull(projectService.removeOneByIndex(index));
    }

    @Test
    public void testRemoveAll() {
        projectService.removeAll();
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void testRemoveAllByUserIdPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            projectService.removeAllByUserId(userId);
            Assert.assertEquals(0, projectService.getSize(userId));
        }
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void testRemoveAllByUserIdNegative() {
        projectService.removeAllByUserId("Other_user_id");
        Assert.assertEquals(projectList.size(), projectService.getSize());
    }

    @Test
    public void testUpdateByIdPositive() {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            @NotNull final String name = "Project Test Update " + id;
            @NotNull final String description = "Description Test Update " + id;
            @Nullable final Project actualProject = projectService.updateById(userId, id, name, description);
            Assert.assertEquals(name, actualProject.getName());
            Assert.assertEquals(description, actualProject.getDescription());
            Assert.assertEquals(project, actualProject);
        }
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateByIdNegative() {
        @NotNull final String userId = "Other_User_Id";
        @NotNull final String id = projectList.get(0).getId();
        @NotNull final String name = "Project Test Update " + id;
        @NotNull final String description = "Description Test Update " + id;
        Assert.assertNotNull(projectService.updateById(userId, id, name, description));
    }

    @Test
    public void testUpdateByIndexPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> projects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < projects.size(); i++) {
                @NotNull final String name = "Project Test Update " + i;
                @NotNull final String description = "Description Test Update " + i;
                Assert.assertNotNull(projectService.findOneByIndex(userId, i));
                @Nullable final Project actualProject = projectService.updateByIndex(userId, i, name, description);
                Assert.assertEquals(name, actualProject.getName());
                Assert.assertEquals(description, actualProject.getDescription());
                Assert.assertEquals(projects.get(i), actualProject);
            }
        }
    }

}
