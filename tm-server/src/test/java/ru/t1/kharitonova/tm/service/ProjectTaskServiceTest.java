package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.api.service.IProjectService;
import ru.t1.kharitonova.tm.api.service.IProjectTaskService;
import ru.t1.kharitonova.tm.api.service.ITaskService;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.exception.entity.TaskNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.kharitonova.tm.exception.field.TaskIdEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.repository.ProjectRepository;
import ru.t1.kharitonova.tm.repository.TaskRepository;

import java.util.UUID;

public class ProjectTaskServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    final String userId = UUID.randomUUID().toString();

    @NotNull
    final String projectName = UUID.randomUUID().toString();

    @NotNull
    final String taskName = UUID.randomUUID().toString();

    @NotNull
    final String description = UUID.randomUUID().toString();

    @Test
    public void testBindTaskToProject() {
        @NotNull final Project project = projectService.create(userId, projectName, description);
        @NotNull final Task task = taskService.create(userId, taskName, description);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject("", project.getId(), task.getId())
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(null, project.getId(), task.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, "", task.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, null, task.getId())
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, project.getId(), "")
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, project.getId(), null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, UUID.randomUUID().toString(), task.getId())
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, project.getId(), UUID.randomUUID().toString())
        );
        projectTaskService.bindTaskToProject(userId, project.getId(), task.getId());
        Assert.assertEquals(task.getProjectId(), project.getId());
    }

    @Test
    public void testRemoveProjectById() {
        @NotNull final Project project = projectService.create(userId, projectName, description);
        @NotNull final Task task = taskService.create(userId, taskName, description);
        projectTaskService.bindTaskToProject(userId, project.getId(), task.getId());
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.removeProjectById("", project.getId())
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.removeProjectById(null, project.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.removeProjectById(userId, null)
        );
        projectTaskService.removeProjectById(userId, project.getId());
        Assert.assertEquals(0, projectService.getSize());
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemoveProjectByIndex() {
        @NotNull final Project project = projectService.create(userId, projectName, description);
        final int tasksLength = 10;
        for (int i = 0; i < tasksLength; i++) {
            Assert.assertEquals(i, taskService.getSize());
            @NotNull final Task task = taskService.create(
                    userId,
                    "Task_" + i,
                    description
            );
            projectTaskService.bindTaskToProject(userId, project.getId(), task.getId());
        }
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.removeProjectByIndex("", 0)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.removeProjectByIndex(null, 0)
        );
        Assert.assertThrows(
                IndexIncorrectException.class,
                () -> projectTaskService.removeProjectByIndex(userId, 1)
        );
        projectTaskService.removeProjectByIndex(userId, 0);
        Assert.assertEquals(0, projectService.getSize());
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testUnbindTaskFromProject() {
        @NotNull final Project project = projectService.create(userId, projectName, description);
        @NotNull final Task task = taskService.create(userId, taskName, description);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject("", project.getId(), task.getId())
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(null, project.getId(), task.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, "", task.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, null, task.getId())
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, project.getId(), "")
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, project.getId(), null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, UUID.randomUUID().toString(), task.getId())
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, project.getId(), UUID.randomUUID().toString())
        );
        projectTaskService.bindTaskToProject(userId, project.getId(), task.getId());
        Assert.assertEquals(task.getProjectId(), project.getId());
        projectTaskService.unbindTaskFromProject(userId, project.getId(), task.getId());
        Assert.assertNull(task.getProjectId());
    }

}
